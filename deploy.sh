#!/bin/env sh
#

srcdir=$(dirname $(readlink -f $0))/src
vimdir=$HOME/.vim

echo "$srcdir -> $vimdir"
if [ ! -d $vimdir ]; then
  mkdir -p $vimdir
fi

if [ ! -d $vimdir/ftdetect ]; then
  mkdir -p $vimdir/ftdetect
fi

if [ ! -d $vimdir/syntax ]; then
  mkdir -p $vimdir/syntax
fi

cp -bf $srcdir/syntax/sfz.vim $vimdir/syntax/sfz.vim
cp -bf $srcdir/ftdetect/sfz.vim $vimdir/ftdetect/sfz.vim
