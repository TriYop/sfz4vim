" Vim syntax file
" Language: Sforzando soundfont
" Maintainer: Yvan Janet
" Latest Revision: 
"
if  exists("b:current_syntax")
  finish
endif

" tags
syn region tags start="<" end=">" contains=control,curve,effect,global,group,region,master

syn region lComment start="//" end="$"
syn match bComment /\/\*.*\*\//


syn match integer "\d\+$" contained
syn region string start="\"" end="\"" contained
syn region include start="#" end="$" contains=string

syn match ccInfo /\(set\|label\|delay\)_cc\d\+/
syn match ccParam "\(lo\|hi\)cc\d\+"


syn keyword sampledef sample end tune volume loop_mode loop_start loop_end seq_position seq_length group off_mode off_by off_time offset 
syn keyword inputctrl lokey hikey lovel hivel pitch_keycenter key
syn keyword pitchctrl am_octaves am_octaves_onccX
syn keyword ampctrl amp_keycenter amp_keytrack amp_random amp_velcurve_127 amp_velcurve_1 amp_veltrack
syn keyword ampegctrl ampeg_attack ampeg_attack_onccN ampeg_decay ampeg_decay_onccN ampeg_delay ampeg_delay_onccN ampeg_hold ampeg_hold_onccN ampeg_release ampeg_start ampeg_start_onccN ampeg_sustain ampeg_sustain_onccN ampeg_vel2attack ampeg_vel2decay ampeg_vel2delay ampeg_vel2hold ampeg_vel2release ampeg_vel2sustain
syn keyword miscctrl apf_lp bpf_1p bpf_2p bpf_2p_sv brf_1p brf_2p brf_2p_sv buffer_destination buffer_source comb count
syn keyword lfoctrl amplfo_delay amplfo_depth amplfo_depthccN amplfo_depthchanaft amplfo_polyaft amplfo_fade amplfo_freq amplfo_freqccN amplfo_freqchanaft amplfo_freqpolyaft
syn keyword autopan autopan_depth autopan_depth_onccX autopan_mode autopan_phase autopan_phase_onccX
syn keyword routing aux1 aux2 aux3 aux4 aux5 aux6 aux7 aux8 bus
syn keyword bendctrl bend_down bend_step bend_up
syn keyword compression comp_attack comp_attack_onccX comp_ratio comp_ratio_onccX
syn keyword cutoffctl cutoff2 cutoff2_curveccN cutoff2_onccN cutoff2_stepccN cutoff cutoff_chanaft cutoff_curveccN cutoff_onccN cutoff_polyaft cutoff_smoothcc cutoff_stepccN
syn keyword equalizer eq1_freq eq1_bw eq1_gaincc48 eq2_freq eq2_bw eq2_gaincc48 eq3_freq eq3_bw eq3_gaincc48


" syn keyword effxsctrl 
syn match operator "\v\="



let b:current_syntax = "sfz"
hi def link tags Keyword
hi def link bComment Comment
hi def link lComment Comment

hi def link sampledef Include
hi def link incluudes Preproc

hi def link inputctrl Constant
hi def link ccInfo Constant
hi def link ccParam Constant

hi def link pitchctrl Identifier
hi def link hctrl Identifier
hi def link ampctrl Identifier
hi def link ampegctrl Identifier
hi def link lfoctrl Identifier
hi def link autopan Identifier
hi def link routing Identifier
hi def link bendctrl Identifier
hi def link compression Identifier
hi def link cutoffctl Identifier

hi def link equalizer Include

hi def link operator Operator

