# sfz4vim

Sforzando soundfonts syntax highlight for vim.

Syntax reference is available here: https://web.archive.org/web/20141109193111/http://betaforum.cakewalk.com/DevXchange/article.aspx?aid=108

## Install

To install sfz syntax to your vim install, you should clone this repository then run deploy script from your local directory.

    git clone --depth 1 https://gitlab.com/TriYop/sfz4v
    sh ./sfz4vim/deploy.sh

## Usage

Whenever you open files with .sfz extension, vim should automatically set syntax to sfz.

## License

    Copyright (C) 2021 by Yvan JANET <dev@yvanjanet.net>

    Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


